import React, { Component } from 'react';
import logo from '../static/logo.svg';
import { Link} from "react-router-dom"; 

export class Navbar extends Component {
    render() {
        let discover = "nav-item " + this.props.activeDiscover;
        let saved = "nav-item " + this.props.activeSaved;
        console.log(discover)

        return (
        <nav className="container-fluid"><div className="navbar navbar-expand-md navbar-dark fixed-top navbg" >
            <img src={logo} width="auto" height="20" alt="" />
            {/* <form className="form-inline mx-auto">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />            
            </form> */}
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav ml-auto">
                    <li className={discover}>
                        <Link className="nav-link" to="/" >Discover</Link><span className="sr-only">(current)</span>
                    </li>
                    <li className={saved}>
                        <Link className="nav-link"  to="/saved"> Saved</Link>
                    </li>
                </ul>
            </div>                
            </div></nav>   
        )
    }
}


export default Navbar;
