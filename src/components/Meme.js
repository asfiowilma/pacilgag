import React, { Component } from 'react'
import SaveMeme from './SaveMeme';
import DelMeme from './DelMeme';


export class Meme extends Component {
  action() {
    if (this.props.isSaved) {
      return (
        <DelMeme postLink={this.props.postLink} delMeme={this.props.delMeme} meme={this.props.meme}/>
      );
    } else {
      return(
        <SaveMeme saveMeme={this.props.saveMeme} meme={this.props.meme}/>
      );
    }
  }

    render() { 
      const { url, title, subreddit } = this.props.meme;
        return (
            <div className="card masonry-brick col-lg-4 col-md-6">
            <img src={url} className="card-img-top" alt="meme" />
            <div className="card-body">
              {this.action()}                           
              <h5 className="card-title">{title}</h5>
              <p className="card-text">from {subreddit}</p>
            </div>
          </div>
        )
    }
}

export default Meme
