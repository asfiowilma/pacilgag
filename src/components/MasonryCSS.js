import styled from "styled-components"

export const MasonryCSS = styled.div`
  display: flex;
  flex-flow: column wrap;
  width: 100%;
  
  @media only screen and (min-width: 768px) {
    max-height: ${props => props.maxHeightLG}px;
  }

  /* Small devices (tablets, 768px and up) */
  @media only screen and (max-width: 768px) {
    max-height: ${props => props.maxHeightMD}px;
  }

  @media only screen and (max-width: 576px) {
    max-height: none;
  }
  
  .masonry-brick {
    margin: 0 1rem 1rem 0; /* Some gutter */
    padding: 0;
  }
`
export default MasonryCSS;