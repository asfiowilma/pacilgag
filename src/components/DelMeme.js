import React, { Component } from 'react'

export class DelMeme extends Component {
    render() {
        const postLink = this.props.postLink;
        return (
            <button className="btn btn-warning btn-sm float-right" type="button" onClick={this.props.delMeme.bind(this, postLink)}>Save</button>
        )
    }
}

export default DelMeme;
