import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './App.css';
import Saved from './pages/Saved';
import Discover from './pages/Discover';

import MasonryCSS from './components/MasonryCSS';

import Navbar from './layout/Navbar';


import axios from 'axios';

class App extends Component {
  state = {
    meme: [],
    saved: []
  }  

  // loads existing saved memes if any
  // gets a randomized memes from api
  componentDidMount() {
    axios.get('https://meme-api.herokuapp.com/gimme')
    .then( res => {this.setState({meme: res.data});})
    const saved = localStorage.getItem('saved');
    let arr = [];
    if (saved) {
      arr = JSON.parse(saved);
      this.setState({ saved: arr });
    }    
  }

  // saves saved memes to local storage upon reloading
  componentDidUpdate() {
    localStorage.setItem('saved', JSON.stringify(this.state.saved));
  }
  
  // saves memes
  saveMeme = () => {
    this.setState({ saved: [...this.state.saved, 
      this.state.meme]});
  }

  // delete saved memes
  delMeme = (postLink) => {
    this.setState({ saved: [...this.state.saved.filter( saved => saved.postLink !== postLink )] });
  }

  render() {
    var isSaved = false;
    
    if (this.state.saved.includes(this.state.meme)) {
      isSaved = true;
    }

    var heightSavedLG = (this.state.saved.length + 3) / 3 * 500;
    var heightSavedMD = (this.state.saved.length + 1) / 2 * 500;

    return ( 
      <Router>
      <div className="App">        
        <Route exact path="/" render= { props => (
          <React.Fragment>
            <Navbar activeDiscover="active" activeSaved="" />
            <Discover meme={this.state.meme} saveMeme={this.saveMeme} delMeme={this.delMeme} isSaved={isSaved}/>
          </React.Fragment>
        )} />
        <Route path="/saved" render= {props => (
          <React.Fragment>
            <Navbar activeDiscover="" activeSaved="active" />
            <div className="container main">
              <h3 className="searchres text-center mt-5">Saved Memes</h3>
              <p className="text-center slug">These are the memes that you love</p>
              <p style={{fontSize:'1rem'}}>*HINT: click again on the 'Save' button to unsave.</p>
                <MasonryCSS maxHeightLG={heightSavedLG} maxHeightMD={heightSavedMD} >
                  <Saved saved={this.state.saved} delMeme={this.delMeme} isSaved={isSaved}/>
                </MasonryCSS>
            </div>
          </React.Fragment>
        )} />        
      </div>
      </Router>
    );
  }
}

export default App;
