import React, { Component } from 'react'
import Meme from '../components/Meme';

export class Discover extends Component {
    render() {
        return (
            <div className="container main">
              <h3 className="searchres text-center mt-5">Discover</h3>
              <div className="text-center">
                <a href="/" className="btn btn-sm btn-warning mb-2">discover more</a>
              </div>
              <div className="row">
                <div className="col-lg-4 col-md-3"> </div>
                <Meme postLink={this.props.meme.postLink} meme={this.props.meme} saveMeme={this.props.saveMeme} delMeme={this.props.delMeme} isSaved={this.props.isSaved}/>
              </div>
            </div>
        )
    }
}

export default Discover
