import React, { Component } from 'react'
import Meme from '../components/Meme';


export class Saved extends Component {    
    render() {
        const isSaved = true;
        return this.props.saved.map ( (saved) => (
            <Meme postLink={saved.postLink} meme={saved} delMeme={this.props.delMeme} isSaved={isSaved}/>
        ));
    }
}

export default Saved;
