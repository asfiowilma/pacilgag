import React, { Component } from 'react'
import Meme from '../components/Meme';

export class Discover extends Component {
    render() {
        return (
            <div className="container main">
              <h3 className="searchres text-center">Search Result</h3>
              <div className="row">
                <Meme meme={this.props.meme} saveMeme={this.props.saveMeme} isSaved={this.props.isSaved}/>
              </div>
            </div>
        )
    }
}

export default Discover
