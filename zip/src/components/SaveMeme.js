import React, { Component } from 'react'

export class SaveMeme extends Component {
    render() {
        // const { postlink, url, title, subreddit } = this.props.meme;
        return (
            <button className="btn btn-outline-warning btn-sm float-right" type="button" onClick={this.props.saveMeme}>Save</button>
        )
    }
}

export default SaveMeme
