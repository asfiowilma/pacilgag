import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom"; 

import './App.css';
import Saved from './pages/Saved';
import Discover from './pages/Discover';

import Navbar from './layout/Navbar';


import axios from 'axios';

class App extends Component {
  state = {
    meme: [],
    saved: []
  }  

  // loads existing saved memes if any
  // gets a randomized memes from api
  componentDidMount() {
    axios.get('https://meme-api.herokuapp.com/gimme')
    .then( res => {this.setState({meme: res.data});})
    const saved = localStorage.getItem('saved');
    let arr = [];
    if (saved) {
      arr = JSON.parse(saved);
      this.setState({ saved: arr });
    }    
  }

  // saves saved memes to local storage upon reloading
  componentDidUpdate() {
    localStorage.setItem('saved', JSON.stringify(this.state.saved));
  }
  
  // saves memes
  saveMeme = () => {
    this.setState({ saved: [...this.state.saved, 
      this.state.meme]});
  }

  // delete saved memes
  delMeme = (postLink) => {
    this.setState({ saved: [...this.state.saved.filter( saved => saved.postLink !== postLink )] });
  }

  render() {
    const isSaved = false;
    return ( 
      <Router>
      <div className="App">
        <Navbar />
        <Route exact path="/" render= { props => (
          <Discover meme={this.state.meme} saveMeme={this.saveMeme} isSaved={isSaved}/>
        )} />
        <Route path="/saved" render= {props => (
          <React.Fragment>
            <div className="container main">
            <h3 className="searchres text-center">Saved Memes</h3>
            <p className="text-center slug">These are the memes that you love</p>
              <div className="row">
                <div className="d-flex flex-row flex-wrap justify-content-start align-items-start">
                  <Saved saved={this.state.saved} delMeme={this.delMeme} isSaved={isSaved}/>
                </div>
              </div>
            </div>
          </React.Fragment>
        )} />        
      </div>
      </Router>
    );
  }
}

export default App;
