import React, { Component } from 'react';
import logo from '../static/logo.svg';
import { Link} from "react-router-dom"; 



export class Navbar extends Component {
    render() {
        return (
        <nav><div className="navbar navbar-expand-lg navbar-dark fixed-top" >
            <img src={logo} width="auto" height="20" alt="" />
            <form className="form-inline mx-auto">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />            
            </form>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item active">
                    <Link className="nav-link" to="/" >Discover</Link><span className="sr-only">(current)</span>
                </li>
                <li className="nav-item">
                    <Link className="nav-link"  to="/saved"> Saved</Link>
                </li>
                </ul>
            </div>                
            </div></nav>   
        )
    }
}


export default Navbar;
